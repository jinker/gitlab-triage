require 'spec_helper'

require 'gitlab/triage/resource/base'

describe Gitlab::Triage::Resource::Base do
  include_context 'network'

  let(:model) { Class.new(described_class) }
  let(:name) { 'Gitlab::Triage::Resource::TestModel' }

  let(:base_url) { "#{net[:host_url]}/api/#{net[:api_version]}" }

  before do
    allow(model).to receive(:name).and_return(name)
  end

  subject { model.new(resource, net) }

  describe '#url' do
    context 'when resource has project_id' do
      let(:resource) { { project_id: 1 } }

      it 'generates the url based on class name and project source' do
        url = subject.__send__(:url)

        expect(url).to eq("#{base_url}/projects/1/test_models?per_page=100")
      end

      it 'generates the url with extra query params' do
        url = subject.__send__(:url, state: 'active')

        expect(url).to eq(
          "#{base_url}/projects/1/test_models?per_page=100&state=active")
      end
    end

    context 'when resource has group_id' do
      let(:resource) { { group_id: 2 } }

      it 'generates the url based on class name and project source' do
        url = subject.__send__(:url)

        expect(url).to eq(
          'http://test.com/api/v4/groups/2/test_models?per_page=100')
      end
    end
  end
end
