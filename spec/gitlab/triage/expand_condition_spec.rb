require 'spec_helper'

require 'gitlab/triage/expand_condition'

describe Gitlab::Triage::ExpandCondition do
  describe '.perform' do
    it 'performs with label sequence expansion' do
      conditions = {
        state: 'opened',
        labels: %w[missed:{0..1} missed:{2..3}]
      }

      labels = []

      subject.perform(conditions, [subject::Sequence]) do |new_conditions|
        labels << new_conditions[:labels]
      end

      expect(labels).to eq(
        [
          %w[missed:0 missed:2],
          %w[missed:0 missed:3],
          %w[missed:1 missed:2],
          %w[missed:1 missed:3]
        ]
      )
    end
  end
end
