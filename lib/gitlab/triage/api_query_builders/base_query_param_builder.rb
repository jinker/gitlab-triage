module Gitlab
  module Triage
    module APIQueryBuilders
      class BaseQueryParamBuilder
        attr_reader :param_name, :param_contents

        def initialize(param_name, param_contents)
          @param_name = param_name
          @param_contents = param_contents
        end

        def build_param
          "&#{param_name}=#{param_content}"
        end
      end
    end
  end
end
