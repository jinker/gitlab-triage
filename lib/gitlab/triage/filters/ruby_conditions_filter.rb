require_relative 'base_conditions_filter'
require_relative '../resource/context'
require 'date'

module Gitlab
  module Triage
    module Filters
      class RubyConditionsFilter < BaseConditionsFilter
        def self.limiter_parameters
          [{ name: :ruby, type: String }]
        end

        def initialize(resource, condition, net = {})
          super(resource, condition)

          @net = net
        end

        def calculate
          !!Resource::Context.new(@resource, @net).eval(@expression)
        end

        private

        def initialize_variables(condition)
          @expression = condition[:ruby]
        end
      end
    end
  end
end
