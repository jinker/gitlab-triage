require_relative 'sequence/expansion'

module Gitlab
  module Triage
    module ExpandCondition
      module Sequence
        def self.expand(conditions)
          labels = conditions[:labels]

          return conditions unless labels

          Expansion.perform(labels).map do |new_labels|
            conditions.merge(labels: new_labels)
          end
        end
      end
    end
  end
end
