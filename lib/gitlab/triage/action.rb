require_relative 'action/summarize'
require_relative 'action/comment'

module Gitlab
  module Triage
    module Action
      def self.process(rules:, **args)
        summarize = rules.delete(:summarize)
        comment = rules.any? && rules

        {
          Summarize => summarize,
          Comment => comment
        }.compact.each do |action, rule|
          act(action: action, rule: rule, **args) if rule
        end
      end

      def self.act(action:, dry:, **args)
        klass =
          if dry
            action.const_get(:Dry)
          else
            action
          end

        klass.new(**args).act
      end
    end
  end
end
