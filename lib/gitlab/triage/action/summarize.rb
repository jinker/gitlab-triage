# frozen_string_literal: true

require_relative 'base'
require_relative 'summarize/issue_builder'

module Gitlab
  module Triage
    module Action
      class Summarize < Base
        class Dry < Summarize
          private

          def perform
            puts "\nThe following issue would be created for the rule **#{name}**:\n\n"
            puts ">>>"
            puts "* Title: #{issue.title}"
            puts "* Description: #{issue.description}"
            puts ">>>"
          end
        end

        def act
          perform if resources.any? && issue.valid?
        end

        private

        def perform
          net[:network].post_api(net[:token], post_issue_url, post_issue_body)
        end

        def issue
          @issue ||= IssueBuilder.new(rule, resources, net)
        end

        def post_issue_url
          # POST /projects/:id/issues
          # https://docs.gitlab.com/ee/api/issues.html#new-issue
          post_url = UrlBuilders::UrlBuilder.new(
            host_url: net[:host_url],
            api_version: net[:api_version],
            source_id: net[:source_id],
            resource_type: 'issues'
          ).build

          puts Gitlab::Triage::UI.debug "post_issue_url: #{post_url}" if net[:debug]

          post_url
        end

        def post_issue_body
          {
            title: issue.title,
            description: issue.description
          }
        end
      end
    end
  end
end
